use bevy::prelude::*;

use crate::rich_dialog::components::*;
use crate::rich_dialog::styles::*;

pub fn on_enter_dialog(mut commands: Commands, asset_server: Res<AssetServer>) {
    info!("Entering rich dialog demo");
    build_dialog_demo(&mut commands, &asset_server);
}

pub fn on_exit_dialog(
    mut commands: Commands,
    rich_dialog_query: Query<Entity, With<RichDialog>>,
) {
    if let Ok(rich_dialog_entity) = rich_dialog_query.get_single() {
        commands.entity(rich_dialog_entity).despawn_recursive();
    }
}

pub fn get_empty_word() -> NodeBundle {
    NodeBundle {
        style: WORD_STYLE,
        //background_color: Color::BLUE.into(),
        ..default()
    }
}

pub fn build_dialog_demo(commands: &mut Commands, asset_server: &Res<AssetServer>) -> Entity {
    let rich_dialog_entity = commands
        .spawn((
            NodeBundle {
                style: MAIN_STYLE,
                ..default()
            },
            RichDialog,
        ))
        .with_children(|parent| {
            // === Dialog box ===
            parent
                .spawn((
                    NodeBundle {
                        style: DIALOG_BOX_STYLE,
                        //background_color: Color::GREEN.into(),
                        ..default()
                    },
                    RichDialogBox,
                ))
                .with_children(|parent| {
                    // === Words ===
                    parent.spawn(get_empty_word());
                });
            // === Next Button ===
            parent
                .spawn((
                    ButtonBundle {
                        style: BUTTON_STYLE,
                        background_color: NORMAL_BUTTON_COLOR.into(),
                        ..default()
                    },
                    NextButton,
                ))
                .with_children(|parent| {
                    parent.spawn(ImageBundle {
                        style: IMAGE_STYLE,
                        image: asset_server.load("sprites/arrow_down.png").into(),
                        ..default()
                    });
                });
        })
        .id();

    rich_dialog_entity
}
