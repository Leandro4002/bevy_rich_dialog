use bevy::prelude::*;

use crate::rich_dialog::components::*;
use crate::rich_dialog::resources::*;
use crate::rich_dialog::styles::*;

use super::layout::get_empty_word;

pub fn interact_with_next_button(
    mut commands: Commands,
    mut button_query: Query<
        (&Interaction, &mut BackgroundColor),
        (Changed<Interaction>, With<NextButton>),
    >,
    mut current_dialog_text: ResMut<RichDialogCurrentText>,
    mut dialog_input: ResMut<RichDialogInput>,
    mut rd_data: ResMut<RichDialogData>,
    rd_config: Res<RichDialogConfiguration>,
    dialog_box: Query<(Entity, &Children), With<RichDialogBox>>,
) {
    if let Ok((interaction, mut background_color)) = button_query.get_single_mut() {
        match *interaction {
            Interaction::Pressed => {
                *background_color = PRESSED_BUTTON_COLOR.into();
                if dialog_input.texts.is_empty() {
                    return;
                }

                // Remove all words
                if let Ok((entity, words_children)) = dialog_box.get_single() {
                    // Remove all words
                    for word in words_children {
                        commands.entity(*word).despawn_recursive();
                    }

                    // Add an empty word
                    let empty_word = commands.spawn(get_empty_word()).id();
                    commands.entity(entity).push_children(&[empty_word]);
                }
                
                rd_data.reset(&rd_config);

                current_dialog_text.0 = dialog_input.texts.remove(0);
            }
            Interaction::Hovered => {
                *background_color = HOVERED_BUTTON_COLOR.into();
            }
            Interaction::None => {
                *background_color = NORMAL_BUTTON_COLOR.into();
            }
        }
    }
}
