use std::time::Duration;

use bevy::prelude::*;

use crate::rich_dialog::components::*;
use crate::rich_dialog::resources::*;

use super::layout::get_empty_word;

pub fn test_event(
    mut rd_event: EventReader<RichDialogEvent>,
) {
    for event in rd_event.read() {
        info!("Received char action event \"{}\"", event.0);
    }
}

pub fn animate_letter(
    mut waving_query: Query<&mut Style, With<LetterWaving>>,
    time: Res<Time>,
    rd_config: Res<RichDialogConfiguration>,
) {
    let mut i = 0;

    for mut style in waving_query.iter_mut() {
        let val = ((time.elapsed_seconds() + i as f32 * 0.1) * rd_config.hover_speed).sin()
            * rd_config.hover_scale;
        style.margin.bottom = Val::Px(val + rd_config.hover_yoffset);

        i += 1;
    }
}

pub fn insert_letter(
    mut commands: Commands,
    asset_server: Res<AssetServer>,
    time: Res<Time>,
    mut next_letter_timer: ResMut<RichDialogLetterTimer>,
    mut rd_data: ResMut<RichDialogData>,
    rd_config: Res<RichDialogConfiguration>,
    mut dialog_text: ResMut<RichDialogCurrentText>,
    dialog_entity: Query<(Entity, &Children), With<RichDialogBox>>,
    mut event_writer: EventWriter<RichDialogEvent>,
) {
    macro_rules! timer_reset {
        ($t:expr) => {
            next_letter_timer.reset();
            next_letter_timer.set_duration(Duration::from_millis($t));
        };
    }

    if dialog_text.is_changed() {
        timer_reset!(rd_data.letter_delay_ms);
    }

    if next_letter_timer.tick(time.delta()).just_finished() {
        if let Ok((entity, words_children)) = dialog_entity.get_single() {
            // If there are no more letters, do nothing
            if dialog_text.0.is_empty() {
                return;
            }

            // Pop the first letter from the dialog text
            let letter = dialog_text.0.remove(0);

            let mut current_letter_delay_ms = rd_data.letter_delay_ms;

            macro_rules! add_letter {
                () => {
                    // Get last child of dialog entity
                    let last_word = words_children
                        .last()
                        .expect("Dialog box should always have at least one child");

                    commands.spawn(AudioBundle {
                        source: asset_server.load("audio/pop.ogg"),
                        settings: PlaybackSettings::DESPAWN,
                    });

                    // Spawn a text entity with the letter
                    let letter_entity = commands
                        .spawn(TextBundle {
                            //background_color: Color::RED.into(),
                            text: Text {
                                sections: vec![TextSection::new(
                                    letter,
                                    TextStyle {
                                        font: asset_server.load(rd_config.font_family),
                                        font_size: rd_data.font_size,
                                        color: rd_data.color,
                                    },
                                )],
                                alignment: TextAlignment::Center,
                                ..default()
                            },
                            ..default()
                        })
                        .id();

                    if rd_data.is_waving {
                        commands.entity(letter_entity).insert(LetterWaving);
                    }

                    commands.entity(*last_word).push_children(&[letter_entity]);
                };
            }

            match letter {
                ' ' => {
                    let empty_word = commands.spawn(get_empty_word()).id();
                    commands.entity(entity).push_children(&[empty_word]);
                    current_letter_delay_ms += rd_config.letter_delay_ms_space;
                    add_letter!();
                }
                '$' => {
                    let color_index = dialog_text.0.remove(0);
                    if let Some(color_index) = color_index.to_digit(10) {
                        rd_data.color = rd_config.colors[color_index as usize];
                    }
                }
                '[' => {
                    let mut parsed_text = String::new();
                    let mut next_char = dialog_text.0.remove(0);
                    while next_char != ']' {
                        parsed_text.push(next_char);
                        next_char = dialog_text.0.remove(0);
                    }
                    event_writer.send(RichDialogEvent(parsed_text.clone()));
                }
                '.' | '!' | '?' | '¿' => {
                    current_letter_delay_ms += rd_config.letter_delay_ms_strong_punctuation;
                    add_letter!();
                }
                ',' | ';' | ':' => {
                    current_letter_delay_ms += rd_config.letter_delay_ms_weak_punctuation;
                    add_letter!();
                }
                '<' => {
                    rd_data.letter_delay_ms += rd_config.letter_delay_ms_step;
                }
                '>' => {
                    // Clamp to avoid overflow
                    if rd_config.letter_delay_ms_step > rd_data.letter_delay_ms {
                        rd_data.letter_delay_ms = 0;
                    } else {
                        rd_data.letter_delay_ms -= rd_config.letter_delay_ms_step;
                    }
                }
                '-' => { /* TODO: */ }
                '+' => { /* TODO: */ }
                '\n' => { /* TODO: */ }
                '*' => { /* TODO: */ }
                '/' => { /* TODO: */ }
                '_' => { /* TODO: */ }
                '%' => { /* TODO: */ }
                '&' => { /* TODO: */ }
                '~' => {
                    rd_data.is_waving = !rd_data.is_waving;
                }
                _ => {
                    add_letter!();
                }
            }
            timer_reset!(current_letter_delay_ms);
        }
    }
}
