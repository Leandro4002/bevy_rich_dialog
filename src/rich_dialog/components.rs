use bevy::prelude::Component;

#[derive(Component)]
pub struct RichDialog;

#[derive(Component)]
pub struct RichDialogBox;

#[derive(Component)]
pub struct NextButton;

#[derive(Component, Clone)]
pub struct LetterWaving;
