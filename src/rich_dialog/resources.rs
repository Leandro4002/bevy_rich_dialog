use bevy::{prelude::*, time::Timer};

#[derive(Resource, Debug)]
pub struct RichDialogCurrentText(pub String);

#[derive(Resource, Debug)]
pub struct RichDialogInput {
    pub texts: Vec<String>,
}

#[derive(Resource, Deref, DerefMut)]
pub struct RichDialogLetterTimer(pub Timer);

// TODO: Make this private, so user of this library know to only use RichDialogConfiguration
#[derive(Resource, Clone)]
pub struct RichDialogData {
    pub font_size: f32,
    pub letter_delay_ms: u64,
    pub is_waving: bool,
    pub is_shaking: bool,
    pub is_pulsating: bool,
    pub color: Color,
}

impl RichDialogData {
    pub fn reset(&mut self, rd_config: &RichDialogConfiguration) {
        self.font_size = rd_config.font_size_base;
        self.letter_delay_ms = rd_config.letter_delay_ms_base;
        self.is_waving = false;
        self.is_shaking = false;
        self.is_pulsating = false;
        self.color = rd_config.colors[0];
    }
}

impl Default for RichDialogData {
    fn default() -> Self {
        Self {
            font_size: 0.,
            letter_delay_ms: 0,
            is_waving: false,
            is_shaking: false,
            is_pulsating: false,
            color: Color::default(),
        }
    }
    
}

#[derive(Event)]
pub struct RichDialogEvent(pub String);

#[derive(Resource)]
pub struct RichDialogConfiguration {
    pub hover_yoffset: f32,
    pub hover_speed : f32,
    pub hover_scale : f32,
    pub pulse_speed: f32,
    pub pulse_scale: f32,
    pub shake_speed: f32,
    pub shake_scale: f32,
    pub font_family: &'static str,
    pub font_family_bold: &'static str,
    pub font_family_italic: &'static str,
    pub font_size_base: f32,
    pub font_size_step: f32,
    pub font_text_speed_base: f32,
    pub font_text_speed_step: f32,
    pub font_text_sound: f32,
    pub colors: [Color; 10],
    pub letter_delay_ms_base: u64,
    pub letter_delay_ms_strong_punctuation: u64,
    pub letter_delay_ms_weak_punctuation: u64,
    pub letter_delay_ms_space: u64,
    pub letter_delay_ms_step: u64,
}

impl Default for RichDialogConfiguration {
    fn default() -> Self {
        Self {
            hover_yoffset: 8.0,
            hover_speed : 2.0,
            hover_scale : 7.0,
            pulse_speed: 1.,
            pulse_scale: 1.,
            shake_speed: 1.,
            shake_scale: 1.,
            font_family: "fonts/BebasNeue-Regular.ttf",
            font_family_bold: "fonts/BebasNeue-Regular.ttf",
            font_family_italic: "fonts/BebasNeue-Regular.ttf",
            font_size_base: 50.,
            font_size_step: 15.,
            font_text_speed_base: 1.,
            font_text_speed_step: 1.2,
            font_text_sound: 0.,
            colors: [
                Color::WHITE,
                Color::RED,
                Color::GREEN,
                Color::YELLOW,
                Color::BLUE,
                Color::PURPLE,
                Color::CYAN,
                Color::BLACK,
                Color::GOLD,
                Color::ORANGE,
            ],
            letter_delay_ms_base: 10,
            letter_delay_ms_strong_punctuation: 1000,
            letter_delay_ms_weak_punctuation: 700,
            letter_delay_ms_space: 30,
            letter_delay_ms_step: 50,
        }
    }
}
