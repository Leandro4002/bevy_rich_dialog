mod components;
mod resources;
mod styles;
mod systems;

use self::resources::*;
use systems::interactions::*;
use systems::layout::*;
use systems::text_animation::*;

use bevy::prelude::*;

use crate::AppState;

pub struct RichDialogPlugin;

impl Plugin for RichDialogPlugin {
    fn build(&self, app: &mut App) {
        app.insert_resource(RichDialogCurrentText("".to_string()))
            .add_event::<RichDialogEvent>()
            .init_resource::<RichDialogData>()
            .init_resource::<RichDialogConfiguration>()
            .insert_resource(RichDialogInput {
                texts: vec![
                    "Search +deep- within ~yourself~ for what you ~truly~ want most.".to_string(),
                    "Think +carefully-, then, without ~shame~ or ~judgment~, [my_event]let your ~heart~ speak your $3wish to the ~stars~.".to_string(),
                    "Close your eyes. <Think about it for real~...~".to_string(),
                ],
            })
            .insert_resource(RichDialogLetterTimer(Timer::from_seconds(0., TimerMode::Once)))
            .add_systems(OnEnter(AppState::RichDialogDemo), on_enter_dialog)
            .add_systems(
                Update,
                (interact_with_next_button, animate_letter, test_event).run_if(in_state(AppState::RichDialogDemo)),
            )
            .add_systems(
                Update,
                (insert_letter.before(interact_with_next_button))
                    .run_if(in_state(AppState::RichDialogDemo)),
            )
            .add_systems(OnExit(AppState::RichDialogDemo), on_exit_dialog);
    }
}
