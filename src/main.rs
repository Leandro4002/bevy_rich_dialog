#![cfg_attr(not(debug_assertions), windows_subsystem = "windows")]

use bevy::{
    prelude::*,
    window::{EnabledButtons, PresentMode, PrimaryWindow, WindowMode},
};
use main_menu::MainMenuPlugin;
use rich_dialog::RichDialogPlugin;

mod main_menu;
mod rich_dialog;

fn main() {
    App::new()
        .insert_resource(ClearColor(Color::rgba_u8(0, 0, 0, 0)))
        // Default plugins
        .add_plugins((
            DefaultPlugins
                .set(WindowPlugin {
                    primary_window: Some(Window {
                        title: "RichDialogDemo".into(),
                        resolution: (960., 540.).into(),
                        resizable: false,
                        enabled_buttons: EnabledButtons {
                            minimize: true,
                            maximize: false,
                            close: true,
                        },
                        present_mode: PresentMode::AutoVsync,

                        ..default()
                    }),
                    ..default()
                })
                .set(ImagePlugin::default_nearest()),
        ))
        .insert_resource(Volume(7))
        // App plugins
        .add_plugins((RichDialogPlugin, MainMenuPlugin))
        .add_state::<AppState>()
        .add_systems(Startup, setup_camera)
        .add_systems(Update, fullscreen)
        .run();
}

fn fullscreen(keys: Res<Input<KeyCode>>, mut q_windows: Query<&mut Window, With<PrimaryWindow>>) {
    if keys.just_pressed(KeyCode::F11) {
        let mut window = q_windows.single_mut();
        if window.mode == WindowMode::Fullscreen {
            window.mode = WindowMode::Windowed;
        } else {
            window.mode = WindowMode::Fullscreen;
        }
    }
}

fn setup_camera(mut commands: Commands) {
    commands.spawn((Camera2dBundle::default(), MainCamera));
}

#[derive(Component)]
pub struct MainCamera;

#[derive(Resource, Debug, Component, PartialEq, Eq, Clone, Copy)]
pub struct Volume(u32);

#[derive(Debug, Clone, Copy, Default, Eq, PartialEq, Hash, States)]
pub enum AppState {
    #[default]
    MainMenu,
    RichDialogDemo,
}
