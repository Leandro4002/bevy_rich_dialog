CONSTANTES
==========
* RDB_HOVER_SCALE
* RDB_HOVER_SPEED
* RDB_HOVER_YOFFSET
* RDB_PULSE_SPEED
* RDB_PULSE_SCALE
* RDB_SHAKE_SPEED
* RDB_SHAKE_SCALE
* RDB_FONT_FAMILY
* RDB_FONT_FAMILY_BOLD
* RDB_FONT_FAMILY_ITALIC
* RDB_FONT_SIZE_BASE
* RDB_FONT_SIZE_STEP
* RDB_TEXT_SPEED_BASE
* RDB_TEXT_SPEED_STEP
* RDB_TEXT_SOUND
* RDB_COLORS[10]

EVENTS CUSTOM
=============
`[my_custom_string]`




// todo add a way to change fonts
// todo? add a rainbow color effect


`~`  : Wave effect  
`%`  : Shaking effect  
`&`  : Pulse effect  
`+`  : Increase font size  
`-`  : Decrease font speed  
`[`  : Start parsing custom event  
`]`  : End parsing custom event  
`$0` : black  
`$1` : red  
`$2` : green  
`$3` : yellow  
`$4` : blue  
`$5` : magenta  
`$6` : cyan  
`$7` : white  
`$8` : gold  
`$9` : orange  
`\n` : Line return  
`*`  : Bold  
`/`  : Italic  
`_`  : Underline  
`>`  : Increase text speed  
`<`  : Decrease text speed  